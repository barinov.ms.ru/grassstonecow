package core;

import graphics.Chank;
import graphics.Direction;

public abstract class ObjectHandler implements IBaseObject{
    int id;
    Chank chank;
    Direction direction;
    boolean exist;

    public ObjectHandler(int id, Chank chank, Direction direction, boolean exist) {
        this.id = id;
        this.chank = chank;
        this.direction = direction;
        this.exist = exist;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public Chank getCoord() {
        return this.chank;
    }

    @Override
    public Direction getDirection() {
        return this.direction;
    }

    @Override
    public boolean isExist() {
        return this.exist;
    }
}
