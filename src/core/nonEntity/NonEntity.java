package core.nonEntity;

import core.ObjectHandler;
import graphics.Chank;
import graphics.Direction;

public class NonEntity extends ObjectHandler {

    public NonEntity(int id, Chank chank, Direction direction, boolean exist) {
        super(id, chank, direction, exist);
    }
}
