package core.nonEntity;

import graphics.Direction;
import java.util.Collections;
import java.util.Set;

public enum NonEntityParam {
    STONE("Stone", 1, Direction.NORTH, Collections.singleton(ObjectSynergy.EAT));

    NonEntityParam(String name, int count, Direction direction, Set<ISynergy> synergy) {
    }
}
