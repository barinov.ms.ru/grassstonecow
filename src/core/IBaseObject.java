package core;

import graphics.Chank;
import graphics.Direction;

public interface IBaseObject {
    int getId();
    Chank getCoord();
    Direction getDirection();
    boolean isExist();
}
